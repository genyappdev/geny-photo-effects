

# Geny Photo Effects
This app is open source for educational purpose. Simple image processing for Android with Android Studio.

* Rotate 90, 180, 270, 360
* Greyscale RGB
* Sephia effects
* Invers Black and White
* Black and White

## General info

Method we are using is simple [image processing](https://en.wikipedia.org/wiki/Digital_image_processing) for educational purpose. Easy to learn for beginner who want to learn about image processing. If you want to get APK file that we released, you can download it on [APKGeny.com](https://www.apkgeny.com/) for free.

# Contributing

I welcome pull requests, issues and feedback.

- Fork it
- Create your feature branch (git checkout -b my-new-feature)
- Commit your changes (git commit -am 'Added some feature')
- Push to the branch (git push origin my-new-feature)
- Create new Pull Request
